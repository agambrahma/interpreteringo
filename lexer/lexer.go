package lexer

import (
	"bitbucket.org/agambrahma/InterpreterInGo/token"
)

// TODO(agam): extend to support Unicode!
type Lexer struct {
	input        string
	position     int  // current char
	readPosition int  // one after current char
	ch           byte // latest char read
}

func NewLexer(input string) *Lexer {
	l := &Lexer{input: input}

	// Ensure valid character to process, before proceeding
	l.readChar()
	return l
}

// Set `ch` and advance read position.
func (l *Lexer) readChar() {
	// When we reach the end, we're 'zeroed' out
	if l.readPosition >= len(l.input) {
		l.ch = 0
	} else {
		l.ch = l.input[l.readPosition]
	}

	l.position = l.readPosition
	l.readPosition++
}

// Get value at read position, do not set `ch`
func (l *Lexer) peekChar() byte {
	if l.readPosition >= len(l.input) {
		return 0
	} else {
		return l.input[l.readPosition]
	}
}

// Helper for peeking
func getTwoChars(l *Lexer) string {
	prevChar := l.ch
	l.readChar()

	return string(prevChar) + string(l.ch)
}

func (l *Lexer) NextToken() token.Token {
	var tok token.Token

	l.skipWhitespace()

	switch l.ch {
	case '=':
		if l.peekChar() == '=' {
			tok = token.Token{token.EQUALS, getTwoChars(l)}
		} else {
			tok = newToken(token.ASSIGNMENT, l.ch)
		}
	case ',':
		tok = newToken(token.COMMA, l.ch)
	case ';':
		tok = newToken(token.SEMICOLON, l.ch)
	case '(':
		tok = newToken(token.LPAREN, l.ch)
	case ')':
		tok = newToken(token.RPAREN, l.ch)
	case '{':
		tok = newToken(token.LBRACE, l.ch)
	case '}':
		tok = newToken(token.RBRACE, l.ch)
	case '+':
		tok = newToken(token.ADD, l.ch)
	case '-':
		tok = newToken(token.MINUS, l.ch)
	case '*':
		tok = newToken(token.MULT, l.ch)
	case '!':
		if l.peekChar() == '=' {
			tok = token.Token{token.NOTEQUALS, getTwoChars(l)}
		} else {
			tok = newToken(token.BANG, l.ch)
		}
	case '<':
		tok = newToken(token.LT, l.ch)
	case '>':
		tok = newToken(token.GT, l.ch)
	case '/':
		tok = newToken(token.SLASH, l.ch)
	case 0:
		tok = token.Token{token.EOF, ""}
	default:
		if isLetter(l.ch) {
			tok.Literal = l.readIdentifier()
			tok.Type = token.LookupIdent(tok.Literal)
			// Skip the extra `readChar`
			return tok
		} else if isDigit(l.ch) {
			tok.Literal = l.readDigits()
			tok.Type = token.INT

			// Skip the extra `readChar`
			return tok
		} else {
			tok = newToken(token.INVALID, l.ch)
		}
	}

	l.readChar()
	return tok
}

func newToken(typ token.TokenType, ch byte) token.Token {
	return token.Token{typ, string(ch)}
}

func isLetter(ch byte) bool {
	return ('a' <= ch && ch <= 'z') ||
		('A' <= ch && ch <= 'Z') ||
		ch == '_'
}

func (l *Lexer) readIdentifier() string {
	startPos := l.position

	for isLetter(l.ch) {
		// Keep going as long as we have letters
		l.readChar()
	}

	// Return what we read
	return l.input[startPos:l.position]
}

func (l *Lexer) skipWhitespace() {
	for l.ch == '\r' || l.ch == '\n' || l.ch == '\t' || l.ch == ' ' {
		l.readChar()
	}
}

func isDigit(ch byte) bool {
	return '0' <= ch && ch <= '9'
}

func (l *Lexer) readDigits() string {
	startPos := l.position

	for isDigit(l.ch) {
		// Keep going as long as we have digits
		l.readChar()
	}

	// Return what we read
	return l.input[startPos:l.position]
}
