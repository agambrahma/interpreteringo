package lexer

import (
	"testing"

	"bitbucket.org/agambrahma/InterpreterInGo/token"
)

func validate(input string, expected []token.Token, t *testing.T) {
	l := NewLexer(input)

	for i, tt := range expected {
		tok := l.NextToken()

		if tok.Type != tt.Type {
			t.Fatalf("tests [%d] failed (token): got %v, expected %v",
				i, tok.Type, tt.Type)
		}

		if tok.Literal != tt.Literal {
			t.Fatalf("tests [%d] failed (literal): got %v, expected %v",
				i, tok.Literal, tt.Literal)
		}
	}
}

func TestNextTokenSingleChar(t *testing.T) {
	input := `=+{}(),;`

	tests := []token.Token{
		{token.ASSIGNMENT, "="},
		{token.ADD, "+"},
		{token.LBRACE, "{"},
		{token.RBRACE, "}"},
		{token.LPAREN, "("},
		{token.RPAREN, ")"},
		{token.COMMA, ","},
		{token.SEMICOLON, ";"},
		{token.EOF, ""},
	}

	validate(input, tests, t)
}

func TestNextTokenSpecialChars(t *testing.T) {
	input := `!-/*5 ; 7 < 10 > 9;`

	tests := []token.Token{
		{token.BANG, "!"},
		{token.MINUS, "-"},
		{token.SLASH, "/"},
		{token.MULT, "*"},
		{token.INT, "5"},
		{token.SEMICOLON, ";"},
		{token.INT, "7"},
		{token.LT, "<"},
		{token.INT, "10"},
		{token.GT, ">"},
		{token.INT, "9"},
		{token.SEMICOLON, ";"},
		{token.EOF, ""},
	}

	validate(input, tests, t)
}

func TestNextTokenConditional(t *testing.T) {
	input := `if (5 < 10) {
    return true
} else {
    return false
}`

	tests := []token.Token{
		{token.IF, "if"},
		{token.LPAREN, "("},
		{token.INT, "5"},
		{token.LT, "<"},
		{token.INT, "10"},
		{token.RPAREN, ")"},
		{token.LBRACE, "{"},
		{token.RETURN, "return"},
		{token.TRUE, "true"},
		{token.RBRACE, "}"},
		{token.ELSE, "else"},
		{token.LBRACE, "{"},
		{token.RETURN, "return"},
		{token.FALSE, "false"},
		{token.RBRACE, "}"},
	}

	validate(input, tests, t)
}

func TestNextTokenSimpleProgram(t *testing.T) {
	input := `let  five = 5;
let ten = 10;

let add = fn(x, y) {
  x + y;
}

let result = add(five, ten);
`

	tests := []token.Token{
		{token.LET, "let"},
		{token.IDENTIFIER, "five"},
		{token.ASSIGNMENT, "="},
		{token.INT, "5"},
		{token.SEMICOLON, ";"},

		{token.LET, "let"},
		{token.IDENTIFIER, "ten"},
		{token.ASSIGNMENT, "="},
		{token.INT, "10"},
		{token.SEMICOLON, ";"},

		{token.LET, "let"},
		{token.IDENTIFIER, "add"},
		{token.ASSIGNMENT, "="},
		{token.FUNC, "fn"},
		{token.LPAREN, "("},
		{token.IDENTIFIER, "x"},
		{token.COMMA, ","},
		{token.IDENTIFIER, "y"},
		{token.RPAREN, ")"},

		{token.LBRACE, "{"},
		{token.IDENTIFIER, "x"},
		{token.ADD, "+"},
		{token.IDENTIFIER, "y"},
		{token.SEMICOLON, ";"},
		{token.RBRACE, "}"},

		{token.LET, "let"},
		{token.IDENTIFIER, "result"},
		{token.ASSIGNMENT, "="},
		{token.IDENTIFIER, "add"},
		{token.LPAREN, "("},
		{token.IDENTIFIER, "five"},
		{token.COMMA, ","},
		{token.IDENTIFIER, "ten"},
		{token.RPAREN, ")"},
		{token.SEMICOLON, ";"},

		{token.EOF, ""},
	}

	validate(input, tests, t)
}

func TestNextTokenDoubleChar(t *testing.T) {
	input := `10 == 20 ; 10 != 20 `

	tests := []token.Token{
		{token.INT, "10"},
		{token.EQUALS, "=="},
		{token.INT, "20"},
		{token.SEMICOLON, ";"},
		{token.INT, "10"},
		{token.NOTEQUALS, "!="},
		{token.INT, "20"},
	}

	validate(input, tests, t)
}


