package main

import (
	"fmt"
	"os"
	"os/user"

	"bitbucket.org/agambrahma/InterpreterInGo/repl"
)

func main() {
	// Personalized intro.
	user, err := user.Current()
	if err != nil {
		panic(err)
	}

	fmt.Printf("Hello, %s! Welcome to the Kong programming language!\n", user.Username)
	fmt.Println("Enter commands below ...\n")

	repl.Start(os.Stdin, os.Stdout)
}
