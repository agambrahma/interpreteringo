package token

type TokenType string

type Token struct {
	Type    TokenType
	Literal string
}

const (
	INVALID = "INVALID"
	EOF     = "EOF"

	// Symbols and literals
	IDENTIFIER = "IDENTIFIER"
	INT        = "INT"

	// Operators
	ASSIGNMENT = "="
	ADD        = "+"
	MULT       = "*"
	BANG       = "!"
	LT         = "<"
	GT         = ">"
	SLASH      = "/"
	MINUS      = "-"

	// Two-letter
	EQUALS    = "=="
	NOTEQUALS = "!="

	// Conditionals
	TRUE   = "true"
	FALSE  = "false"
	IF     = "if"
	ELSE   = "else"
	RETURN = "return"

	// Delimiters
	COMMA     = ","
	SEMICOLON = ";"

	LPAREN = "("
	RPAREN = ")"
	LBRACE = "{"
	RBRACE = "}"

	// Special words
	FUNC = "FUNC"
	LET  = "LET"
)

// Separate set of special keywords
var keywords = map[string]TokenType{
	"fn":     FUNC,
	"let":    LET,
	"true":   TRUE,
	"false":  FALSE,
	"if":     IF,
	"else":   ELSE,
	"return": RETURN,
}

// Check if a word is a keyword
func LookupIdent(word string) TokenType {
	if tok, ok := keywords[word]; ok {
		return tok
	}

	return IDENTIFIER
}
