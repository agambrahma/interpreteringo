package repl

import (
	"bufio"
	"fmt"
	"io"

	"bitbucket.org/agambrahma/InterpreterInGo/lexer"
	"bitbucket.org/agambrahma/InterpreterInGo/token"
)

const PROMPT = ">>"

func Start(in io.Reader, out io.Writer) {
	scanner := bufio.NewScanner(in)

	for {
		// Show prompt at beginning of line.
		fmt.Printf(PROMPT)
		scanned := scanner.Scan()

		if !scanned {
			// Okay, we're done here.
			return
		}

		// Take the next line, and "tokenize" it.
		line := scanner.Text()
		l := lexer.NewLexer(line)

		for tok := l.NextToken(); tok.Type != token.EOF; tok = l.NextToken() {
			fmt.Printf("%v\n", tok)
		}
	}
}
