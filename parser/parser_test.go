package parser

import (
	"testing"
 	"bitbucket.org/agambrahma/InterpreterInGo/ast"
	"bitbucket.org/agambrahma/InterpreterInGo/lexer"
)

func TestLetStatements(t * testing.T) {
	input := `
let x = 7;
let y = 20;
let foobar = 432265;
`
	l := lexer.NewLexer(input)
	p := New(l)

	program := p.ParseProgram()
	if program == nil {
		t.Fatalf("Could not parse program!")
	}
	if len(program.Statements) != 3 {
		t.Fatalf("Incorrect number of statements, expected 3, got %v", len(program.Statements))
	}

	expectedIdentifiers := []string{"x", "y", "foobar"}

	for i, ident := range expectedIdentifiers {
		stmt := program.Statements[i]
		if !testLetStatement(t, stmt, ident) {
			return
		}
	}
}

func testLetStatement(t *testing.T, s ast.Statement, name string) bool {
	if s.TokenLiteral() != "let" {
		t.Errorf("statement token literal: expected 'let', got '%v'", s.TokenLiteral())
		return false
	}

	letStmt, ok := s.(*ast.LetStatement)
	if !ok {
		t.Errorf("expected let statement, got %T", s)
		return false
	}

	if letStmt.Name.Value != name {
		t.Errorf("let statement name value: expected %v, got %v", name, letStmt.Name.Value)
		return false
	}

	if letStmt.Name.TokenLiteral() != name {
		t.Errorf("let statement name token literal: expected %v, got %v", name, letStmt.Name.TokenLiteral())
		return false
	}

	return true
}
