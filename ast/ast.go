package ast

import 	"bitbucket.org/agambrahma/InterpreterInGo/token"

type Node interface {
	TokenLiteral() string
}

type Statement interface {
	Node
	statementNode()
}

type Expression interface {
	Node
	expressionNode()
}

// Programs in our lanugage are composed of statements.
type Program struct {
	Statements []Statement
}

// The program can be treated like a node
func (p *Program) TokenLiteral() string {
	if len(p.Statements) > 0 {
		// Forward to the first statement.
		return p.Statements[0].TokenLiteral()
	} else {
		return ""
	}
}

// Start with the LET statement
type LetStatement struct {
	Token token.Token
	Name *Identifier
	Value Expression
}

// ... which is a statement.
func (ls *LetStatement) statementNode() {}
func (ls *LetStatement) TokenLiteral() string {
	// Forward to the token.
	return ls.Token.Literal
}

// Next, IDENTIFIERs
type Identifier struct {
	Token token.Token
	Value string
}

// ... which is an expression.
func (ident *Identifier) expressionNode() {}
func (ident *Identifier) TokenLiteral() string {
	// Forward to the token
	return ident.Token.Literal
}
